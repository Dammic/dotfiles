FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
  apt-get upgrade -y && \
  apt-get install -y \
  sudo \
  curl \
  git-core \
  gnupg \
  locales \
  tzdata \
  wget && \
  apt-get autoremove -y

# RUN git clone --quiet https://gitlab.com/Dammic/dotfiles.git ~/dotfiles
COPY . /root/dotfiles
RUN bash -c "/root/dotfiles/bin/dotfiles"

ENTRYPOINT ["/bin/zsh"]
