-- Shorter function name
local keymap = vim.api.nvim_set_keymap

local function noremapkey(mode, lhs, rhs)
  vim.api.nvim_set_keymap(mode, lhs, rhs, { noremap = true })
end

local function mapcmd(key, cmd)
  vim.api.nvim_set_keymap("n", key, ":" .. cmd .. "<cr>", { noremap = true })
end

local function maplua(key, txt)
  vim.api.nvim_set_keymap("n", key, ":lua " .. txt .. "<cr>", { noremap = true })
end

-- fzf bindings
maplua("<C-p>", "require('fzf-lua').files()")
maplua("<C-g>", "require('fzf-lua').grep_project()")

-- nvim-tree bindings
maplua("<LEADER>;", "require('nvim-tree.api').tree.toggle()")

-- Open vim config
mapcmd("<LEADER>ev", ":split ~/.config/nvim/init.lua")

-- Treat ctrl-c as esc (useful for esc-less macs)
noremapkey("n","<C-c>", "<Esc>")

-- This is a quick way to call search-and-replace on a current word
keymap("n", "<LEADER>s", [[:%s/<C-r><C-w>//g<Left><Left>]], {})

-- disable vim's autoincrement and autodecrement
keymap("n", "<C-a>", "<Nop>", {})
keymap("n", "<C-x>", "<Nop>", {})
