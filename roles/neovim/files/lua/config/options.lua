local home = os.getenv("HOME")

local options = {
  encoding = "utf8",
  clipboard = "unnamedplus", -- allows neovim to access the system clipboard
  list = true,
  number = true,
  ruler = true,
  cursorline = true,
  linebreak = true,
  wildmenu = true,
  lazyredraw = true,
  showmatch = true,
  showtabline = 2,
  signcolumn = "yes",
  textwidth = 0,
  wrap = false,
  cmdheight = 2,
  autoindent = true,
  smartindent = true,
  smartcase = true,
  ignorecase = true,
  expandtab = true,
  tabstop = 2,
  softtabstop = 2,
  shiftwidth = 2,
  splitbelow = true,
  splitright = true,

  diffopt = "vertical",
  backupcopy = "yes", -- for webpack to catch all writes

  undofile = true, -- Vim will store undo externally, so after closing and reopening file you can undo changes

-- Setting .swp files to be centralized, not clutter the edit folder
  backupdir = home .. "/.vim/backup/",
  directory = home .. "/.vim/swap/",
  undodir = home .. "/.vim/undo/",

  completeopt = { "menu", "menuone", "noselect" },
}

vim.opt.shortmess:append("c")
vim.opt.path:append({ "**" }) -- Finding files - Search down into subfolders

vim.opt.wildignore:append({ "*/node_modules/*" })
vim.opt.rtp:append({ "/usr/local/opt/fzf" })

vim.cmd [[let g:prettier#autoformat = 1]]
vim.cmd [[let g:prettier#autoformat_require_pragma = 0]]

for k, v in pairs(options) do
  vim.opt[k] = v
end
