require("mason").setup()
require("mason-lspconfig").setup()

local lspconfig = require('lspconfig')
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

lspconfig.ts_ls.setup({
  on_attach = on_attach,
  capabilities = capabilities,
})

-- lspconfig.kotlin_language_server.setup({
--   on_attach = on_attach,
--   capabilities = capabilities,
-- })


vim.cmd([[nnoremap gd :lua vim.lsp.buf.definition()<CR>]])
vim.cmd([[nnoremap K :lua vim.lsp.buf.hover()<CR>]])
vim.cmd([[nnoremap <leader>. :lua vim.lsp.buf.code_action()<CR>]])
vim.cmd([[nnoremap <leader>rn :lua vim.lsp.buf.rename()<CR>]])

-- Showing diagnostics on hover instead of inline
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
  vim.lsp.diagnostic.on_publish_diagnostics, {
    virtual_text = false,
    underline = true,
    signs = true,
  }
)

vim.cmd [[autocmd CursorHold * silent! lua vim.diagnostic.open_float({focusable = false})]]
vim.cmd [[autocmd CursorHoldI * silent! lua vim.lsp.buf.signature_help()]]
