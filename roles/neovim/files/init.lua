-- Vimrc file maintained by Damian Michalski for personal use
--
-- NOTE: in order for airline to look good, you need to install and select powerline fonts pack
--       as well as nerd-fonts (this font: brew cask install font-ubuntumono-nerd-font)
--
-- NOTE: markonm/traces.vim is incompatible with neovim - to make it work, turn off inccommand
--
-- NOTE: In order to get clipboard working on linux, install xsel:
--       sudo apt-get install xsel
--
-- Extra: Grab yourself a nice docker thingy here: https://github.com/jesseduffield/lazydocker

require('config/lazy')

require("lazy").setup({
  {
    "catppuccin/nvim",
    name = "catppuccin",
    priority = 1000,
    config = function()
      require("catppuccin").setup({
        flavour = "mocha",
      })
      vim.cmd [[colorscheme catppuccin]]
    end,
  },
  { 'sheerun/vim-polyglot' },
  {
    'echasnovski/mini.nvim',
    version = '*',
    config = function()
      require('mini.icons').setup()
      require('mini.statusline').setup()
      require('mini.ai').setup()
      require('mini.pairs').setup()
      require('mini.splitjoin').setup()
      require('mini.surround').setup()
      require('mini.jump').setup()
      require('mini.tabline').setup()
      require('mini.comment').setup()
    end,
  },
 "psliwka/vim-smoothie",

  {
    "ibhagwan/fzf-lua",
    dependencies = { "echasnovski/mini.icons" },
    opts = {}
  },

  -- Project tree explorer
  {
    "kyazdani42/nvim-tree.lua",
    lazy = false,
    config = function()
      require'nvim-tree'.setup {
       update_focused_file = {
         -- enables the feature
         enable      = true,
         -- update the root directory of the tree to the one of the folder containing the file if the file is not under the current root directory
         -- only relevant when `update_focused_file.enable` is true
         update_cwd  = false,
         update_root = false,
         -- list of buffer names / filetypes that will not update the cwd if the file isn't found under the current root directory
         -- only relevant when `update_focused_file.update_cwd` is true and `update_focused_file.enable` is true
         ignore_list = {}
       },
       renderer = {
         highlight_opened_files = "all",
         indent_markers = {
           enable = true
         },
         highlight_git = true,
         icons = {
           show = {
             file = true,
             folder = true,
             folder_arrow = true,
             git = false,
           },
         },
       },
       view = {
         width = 40,
       },
      }
      local function open_nvim_tree()

        -- open the tree
        require("nvim-tree.api").tree.open()
      end

      vim.api.nvim_create_autocmd({ "VimEnter" }, { callback = open_nvim_tree })
    end,
  },
  -- command for quickly moving in a file
  "tpope/vim-repeat",
  -- An amazing plugin for live preview when executing substitute command
  "markonm/traces.vim",
  -- If you prefer Ctrl+h/j/k/l for navigating across vim/tmux splits,
  -- this plugin will integrate Vim and Tmux, so that you can seamlessly
  -- Jump across the border of a vim/tmux split
  "christoomey/vim-tmux-navigator",
  "williamboman/mason.nvim",
  {
    "williamboman/mason-lspconfig.nvim",
    opts = {
      automatic_installation = true,
      ensure_instaled = { "ts_ls" },
    },
  },

  {
    "hrsh7th/nvim-cmp",
    lazy = false,
    dependencies = {
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-path",
      "hrsh7th/cmp-cmdline",
    },
    config = function()
      local cmp = require('cmp')
      cmp.setup({
        snippet = {
          -- REQUIRED - you must specify a snippet engine
          expand = function(args)
            vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
          end,
        },
        window = {
          completion = cmp.config.window.bordered(),
          documentation = cmp.config.window.bordered(),
        },
        mapping = cmp.mapping.preset.insert({
          ['<C-b>'] = cmp.mapping.scroll_docs(-4),
          ['<C-f>'] = cmp.mapping.scroll_docs(4),
          ['<C-Space>'] = cmp.mapping.complete(),
          ['<C-e>'] = cmp.mapping.abort(),
          ['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
        }),
        sources = cmp.config.sources({
          { name = 'nvim_lsp' },
          { name = 'vsnip' }, -- For vsnip users.
        }, {
          { name = 'buffer' },
        })
      })

      -- Set configuration for specific filetype.
      cmp.setup.filetype('gitcommit', {
        sources = cmp.config.sources({
          { name = 'cmp_git' }, -- You can specify the `cmp_git` source if you were installed it.
        }, {
          { name = 'buffer' },
        })
      })

      -- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
      cmp.setup.cmdline('/', {
        mapping = cmp.mapping.preset.cmdline(),
        sources = {
          { name = 'buffer' }
        }
      })

      -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
      cmp.setup.cmdline(':', {
        mapping = cmp.mapping.preset.cmdline(),
        sources = cmp.config.sources({
          { name = 'path' }
        }, {
          { name = 'cmdline' }
        })
      })
    end,
  },

  {
    "neovim/nvim-lspconfig",
    lazy = false,
  },

  { "nvim-treesitter/nvim-treesitter", build = ":TSUpdate" },

  -- Autoformatting of files, using :Prettier
  { "prettier/vim-prettier", build = "yarn install" },
  { "iamcco/markdown-preview.nvim", build = "cd app && npm install" },
})

require('config/lsp-config')
require('config/options')
require('config/keymaps')
