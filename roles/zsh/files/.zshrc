export PATH="$PATH:/opt/nvim/"
export ZSH_DISABLE_COMPFIX=true

export TERM="xterm-256color"

export ZSH="$HOME/.oh-my-zsh"
export EDITOR="nvim"
export FZF_DEFAULT_COMMAND='ag --hidden --ignore .git -g ""'
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8

SPACESHIP_DOCKER_SHOW=false
SPACESHIP_KUBECONTEXT_SHOW=false
SPACESHIP_CHAR_SYMBOL="λ "
SPACESHIP_GIT_SYMBOL="\ue702 "
SPACESHIP_GIT_BRANCH_COLOR="red"

# TMUX
# Automatically start tmux
ZSH_TMUX_AUTOSTART=true
# Automatically connect to a previous session if it exists
ZSH_TMUX_AUTOCONNECT=true

# Enable command auto-correction.
ENABLE_CORRECTION="true"

# Display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Enabled true color support for terminals
export NVIM_TUI_ENABLE_TRUE_COLOR=1

# Aliases
alias vim="nvim"
alias cdproj="cd /mnt/jetbrains/work/codecanvas"
# alias top="vtop --theme=wizard"
# alias ls="colorls -lA --sd"
# alias gimme-no-cors-chrome="open -n -a /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --args --user-data-dir="/tmp/chrome_dev_test" --disable-web-security"
alias gimme-no-cors-chrome="google-chrome --disable-web-security --user-data-dir=~/chromeTemp"

alias startDockerRosetta="colima start --arch aarch64 --vz-rosetta --vm-type vz --cpu 4 --memory 8"
alias startDocker="colima start --arch aarch64 --vm-type vz --cpu 4 --memory 8"
alias stopDocker="colima stop"

bindkey -v

source ~/.zplug/init.zsh

zplug "spaceship-prompt/spaceship-prompt", use:spaceship.zsh, from:github, as:theme

# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose; then
  zplug install
fi

zplug load --verbose

plugins=(
  git
  docker
  tmux
  vi-mode
)

source $ZSH/oh-my-zsh.sh

ZSH_THEME="spaceship"
fpath=($fpath "/home/dammic/.zfunctions")

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

autoload -U promptinit; promptinit
prompt spaceship

autoload -U add-zsh-hook

zmodload -i zsh/complist
bindkey -M menuselect '^M' .accept-line

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

function gbsForScript {
  git for-each-ref --sort=-committerdate refs/heads/ --format='%(refname:short)' | head -n 25
}

function gon {
  gbsForScript | head -n 15 | nl
  echo 'Which branch: '
  read branch_number
  branch_name=$(gbsForScript | awk "NR==$branch_number" | tr -d ' ')
  git checkout $branch_name
  # update_current_git_vars # remove if not using zsh-git-prompt
}

function cleanDockerImages {
docker images --no-trunc --format '{{.ID}} {{.CreatedSince}}' \
    | grep ' months' | awk '{ print $1 }' \
    | xargs docker rmi
}

fpath=($fpath "/Users/dmichalski/.zfunctions")
